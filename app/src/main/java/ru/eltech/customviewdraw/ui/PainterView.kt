package ru.eltech.customviewdraw.ui

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View

class PainterView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private val paint = Paint().apply {
        color = Color.RED
    }


    private val rectList = mutableListOf<Rect>()
    private val paintList = mutableListOf<Paint>()
    private var drag = false
    var dragX = 0f
    var dragY = 0f
    private var selectedRect: Rect? = null

    var isRandomColor = false

    override fun onTouchEvent(event: MotionEvent): Boolean {

        val evX = event.x
        val evY = event.y

        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                selectedRect = rectList.firstOrNull { it.contains(evX.toInt(), evY.toInt()) }
                if (selectedRect == null) {
                    rectList.add(
                        Rect(
                            (evX - RECT_SIZE / 2).toInt(),
                            (evY - RECT_SIZE / 2).toInt(),
                            (evX + RECT_SIZE / 2).toInt(),
                            (evY + RECT_SIZE / 2).toInt(),
                        )
                    )
                    selectedRect = null
                    val color = Color.rgb((0..255).random(), (0..255).random(), (0..255).random())
//                    paint.apply {
//                        this.color = color
//                    }
                    paintList.add(Paint().apply {
                        this.color = color
                    })
                    invalidate()
                } else {
                    rectList.remove(selectedRect)
                    drag = true
//                    dragX = evX
//                    dragY = evY

                }


                Log.d(
                    "PainterView", """
                    size:
                    left = ${(evX - RECT_SIZE / 2)}
                    top = ${(evY - RECT_SIZE / 2)}
                    right = ${(evX + RECT_SIZE / 2)}
                    bottom = ${(evY + RECT_SIZE / 2)}
                """.trimIndent()
                )
                invalidate()
            }
            MotionEvent.ACTION_MOVE -> {

                if (drag) {

                    // определеяем новые координаты для рисования
                    selectedRect?.let {
                        rectList.remove(it)
                        it.offset((evX - it.centerX()).toInt(), (evY - it.centerY()).toInt())

                        rectList.add(it)
                        invalidate()
                    }
                    Log.d("PainterView", "x = $evX, y = $evY")
                }


            }

            MotionEvent.ACTION_UP -> {
                drag = false
//                dragX = 0f
//                dragY = 0f
                selectedRect = null
                //invalidate()
            }


        }
        return true

    }

    fun undoRect() {
        if (rectList.isNotEmpty()) {
            rectList.removeAt(rectList.size - 1)
            invalidate()
        }
    }

    override fun onDraw(canvas: Canvas?) {
        for ((index, rect) in rectList.withIndex()) {
            if (isRandomColor) {
                canvas?.drawRect(rect, paintList[index])
            } else {
                canvas?.drawRect(rect, paint)
            }
        }
    }

    companion object {
        private const val RECT_SIZE = 100
    }
}
