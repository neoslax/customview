package ru.eltech.customviewdraw

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.CheckBox
import androidx.appcompat.app.AlertDialog
import com.google.android.material.floatingactionbutton.FloatingActionButton
import ru.eltech.customviewdraw.ui.PainterView

class MainActivity : AppCompatActivity() {

    var sharPref: SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        sharPref = getSharedPreferences("myApp", MODE_PRIVATE)

        val painterView = findViewById<PainterView>(R.id.painter)
        val undoBtn = findViewById<FloatingActionButton>(R.id.btn_undo)
        val isRandomCB = findViewById<CheckBox>(R.id.chk_random_color)



        undoBtn.setOnClickListener {
            painterView.undoRect()
        }

        isRandomCB.setOnCheckedChangeListener { buttonView, isChecked ->
            painterView.isRandomColor = isChecked
        }

    }

    override fun onResume() {
        super.onResume()
        if (sharPref?.getBoolean("firstrun", true) == true) {
            showWelcomeMessage()
            sharPref?.edit()?.putBoolean("firstrun", false)?.commit();
        }
    }

    private fun showWelcomeMessage() {
        val alert = AlertDialog.Builder(this)
            .setTitle("Features")
            .setMessage(
                """
        - random colors
        - undo draw
    """.trimIndent()
            )
            .setPositiveButton("Nice!") { _, _ -> }
            .create()
        alert.show()
    }
}